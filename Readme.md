# Lis4381 

## Jensen Dietz

### Lis4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/a1.md "My A1 README.md file")
    - Install AMPPS, JDK & Android Studio
    - Create Bitbucket Repo
    - Complete Bitbucket Tutorials
    - Git commands with descriptions

2. [A2 README.md](a2/a2.md "My A2 README.md file")
    - Create Bruschetta Recipe in Android Studio
    - Add TextView and ImageView in Studio
    - Changed background color theme

3. [A3 README.md](a3/a3.md "My A3 README.md file")
    - Create pet store ERD
    - Create Concert app in Android Studio
    - Create border around image and button
   